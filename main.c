#include <stdio.h>
#include <memory.h>
#include <netinet/in.h>
#include <dlfcn.h>
#include <stdlib.h>


#include "list.h"
#include "library.h"

#define USER_PACKET_COUNT_BUFFER_SIZE 32






int main(int argc, char *argv[]) {

    //library functions pointers
    void (*p_find_interface_and_address_by_user_input_interface_name)(char *p_interface_name,
                                                                      struct in6_addr *p_ip_v6_address) = NULL;

    void (*p_set_ipv6_field_by_user_input)(struct ip6_hdr *p_ip_v6_header) = NULL;
    void (*p_set_ipv6_src_and_dst_addresses_by_user_input)(struct ip6_hdr *p_ip_v6_header,
                                                           struct in6_addr *p_default_address) = NULL;

    void (*p_set_icmp_v6_field_by_user_input)(struct icmp6_hdr *p_icmp_v6_header);
    int (*p_create_icmp_v6_data_by_user_input)(unsigned char **p_icmp_v6_data,
                                               unsigned short *p_icmp_v6_data_size) = NULL;

    int (*p_create_icmp_v6_packet)(struct ip6_hdr *p_ip_v6_header,
                              struct icmp6_hdr *p_icmp_v6_header,
                              unsigned char *p_icmp_v6_data,
                              unsigned short icmp_v6_data_size,
                              unsigned char **p_out_ip_packet,
                              unsigned short *p_out_ip_packet_size) = NULL;

    int (*p_send_ethernet_packet)(char *p_interface_name,
                                  unsigned char *p_ip_packet,
                                  unsigned int ip_packet_size) = NULL;

    //load library
    void *p_icmp_v6_lib_handle = dlopen(".././libicmpv6_lib.so", RTLD_NOW);
    if (p_icmp_v6_lib_handle == NULL){
        printf("Library not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    char interface_name[20];
    memset(interface_name, '\0', 20);
    struct in6_addr default_addr;

    p_find_interface_and_address_by_user_input_interface_name
            = dlsym(p_icmp_v6_lib_handle, "find_interface_and_address_by_user_input_interface_name");
    if (p_find_interface_and_address_by_user_input_interface_name == NULL){
        printf("Function find_interface_and_address_by_user_input_interface_name not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    p_find_interface_and_address_by_user_input_interface_name(interface_name ,&default_addr);

    struct ip6_hdr ip_v6_header;
    memset(&ip_v6_header, 0, sizeof(struct ip6_hdr));

    p_set_ipv6_field_by_user_input
            = dlsym(p_icmp_v6_lib_handle, "set_ipv6_version_by_user_input");
    if (p_set_ipv6_field_by_user_input == NULL){
        printf("Function set_ipv6_version_by_user_input not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    p_set_ipv6_field_by_user_input(&ip_v6_header);

    p_set_ipv6_field_by_user_input
            = dlsym(p_icmp_v6_lib_handle, "set_ipv6_payload_length_by_user_input");
    if (p_set_ipv6_field_by_user_input == NULL){
        printf("Function set_ipv6_payload_length_by_user_input not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    p_set_ipv6_field_by_user_input(&ip_v6_header);

    p_set_ipv6_field_by_user_input
            = dlsym(p_icmp_v6_lib_handle, "set_ipv6_hops_limit_by_user_input");
    if (p_set_ipv6_field_by_user_input == NULL){
        printf("Function set_ipv6_hops_limit_by_user_input not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    p_set_ipv6_field_by_user_input(&ip_v6_header);

    p_set_ipv6_src_and_dst_addresses_by_user_input
            = dlsym(p_icmp_v6_lib_handle, "set_ipv6_src_and_dst_addresses_by_user_input");
    if (p_set_ipv6_src_and_dst_addresses_by_user_input == NULL){
        printf("Function set_ipv6_src_and_dst_addresses_by_user_input not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    p_set_ipv6_src_and_dst_addresses_by_user_input(&ip_v6_header, &default_addr);

    struct icmp6_hdr icmp_v6_header;
    memset(&icmp_v6_header, 0, sizeof(struct icmp6_hdr));

    p_set_icmp_v6_field_by_user_input
            = dlsym(p_icmp_v6_lib_handle, "set_icmp_v6_type_and_code_by_user_input");
    if (p_set_icmp_v6_field_by_user_input == NULL){
        printf("Function set_icmp_v6_type_and_code_by_user_input not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    p_set_icmp_v6_field_by_user_input(&icmp_v6_header);

    p_set_icmp_v6_field_by_user_input
            = dlsym(p_icmp_v6_lib_handle, "set_icmp_v6_message_body_by_user_input");
    if (p_set_icmp_v6_field_by_user_input == NULL){
        printf("Function set_icmp_v6_message_body_by_user_input not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    p_set_icmp_v6_field_by_user_input(&icmp_v6_header);

    unsigned char *p_icmp_v6_data;
    unsigned short icmp_v6_data_size;

    p_create_icmp_v6_data_by_user_input
            = dlsym(p_icmp_v6_lib_handle, "create_icmp_v6_data_by_user_input");
    if (p_create_icmp_v6_data_by_user_input == NULL){
        printf("Function create_icmp_v6_data_by_user_input not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    p_create_icmp_v6_data_by_user_input(&p_icmp_v6_data, &icmp_v6_data_size);

    unsigned char *p_out_ip_packet;
    unsigned short out_ip_packet_size = 0;

    p_create_icmp_v6_packet
            = dlsym(p_icmp_v6_lib_handle, "create_icmp_v6_packet");
    if (p_create_icmp_v6_packet == NULL){
        printf("Function create_icmp_v6_packet not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    p_create_icmp_v6_packet(&ip_v6_header, &icmp_v6_header, p_icmp_v6_data, icmp_v6_data_size, &p_out_ip_packet,
                          &out_ip_packet_size);

    char input_buffer[USER_PACKET_COUNT_BUFFER_SIZE];
    memset(input_buffer, '\0', USER_PACKET_COUNT_BUFFER_SIZE);

    printf("Please specify number of frames to store and send (default is 1)\n");
    fgets(input_buffer, USER_PACKET_COUNT_BUFFER_SIZE, stdin);
    unsigned int frames_count = 1;
    if (strlen(input_buffer) > 1) { frames_count = (unsigned int) strtoul(input_buffer, NULL, 0); }
    if (frames_count < 1) { frames_count = 1; }

    list_t *p_frame_list = create_list();

    printf("Copying frames to list...\n");
    for(int i = 0; i < frames_count; i++){ push_node(p_frame_list, p_out_ip_packet, out_ip_packet_size); }

    p_send_ethernet_packet
            = dlsym(p_icmp_v6_lib_handle, "send_ethernet_packet");
    if (p_send_ethernet_packet == NULL){
        printf("Function send_ethernet_packet not found!\n");
        fputs(dlerror(), stderr);
        return -1;
    }

    printf("Sending frames...\n");
    node_t *p_current_node = p_frame_list->head;

    while (p_current_node != NULL){
        p_send_ethernet_packet(interface_name, p_current_node->p_packet, p_current_node->packet_size);
        p_current_node = p_current_node->next;
    }

    printf("Frames sent, destroying list...\n");
    delete_list(p_frame_list);

    if (dlclose(p_icmp_v6_lib_handle)){
        printf("Library closing error: %s\n", dlerror());
        return -1;
    }

    return EXIT_SUCCESS;
}
