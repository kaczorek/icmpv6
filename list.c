//
// Created by kaczor on 27.11.17.
//

#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

 list_t* create_list(void){
    list_t *p_new_list;
    p_new_list = malloc(sizeof(list_t));
    if(p_new_list == NULL){
        return NULL;
    }
    p_new_list->head = NULL;
    p_new_list->tail = NULL;
    return p_new_list;
}


void delete_list(list_t *p_list){
    node_t *p_current_node = p_list->head;
    node_t *p_next_node = NULL;
    while (p_current_node != NULL){
        p_next_node = p_current_node->next;
        free(p_current_node);
        p_current_node = p_next_node;
    }

    free(p_list);
}


node_t* get_node(list_t *p_list, unsigned int n){
    int iterator = 0;
    node_t *p_current_node = p_list->head;
    if (p_current_node == NULL) {
        return NULL;
    }

    while (iterator != n && p_current_node != NULL){
        iterator++;
        p_current_node = p_current_node->next;
    }

    return p_current_node;
}

unsigned int get_iterator(list_t *p_list){
    unsigned int iterator = 0;
    node_t* p_current_node = p_list->head;
    while (p_current_node->next != NULL){
        p_current_node = p_current_node->next;
        iterator++;
    }
    return iterator;
}


void push_node(list_t *p_list, unsigned char *p_packet, unsigned short packet_size){
    node_t *p_new_node = malloc(sizeof(struct node_t));
    p_new_node->p_packet = malloc(packet_size);
    if (p_packet == NULL){
        printf("List element allocation error\n");
        return;
    }

    memcpy(p_new_node->p_packet, p_packet, packet_size);
    p_new_node->packet_size = packet_size;
    p_new_node->next = NULL;
    p_new_node->prev = NULL;

    //if p_list empty
    if (p_list->tail == NULL){
        p_list->head = p_new_node;
        p_list->tail = p_new_node;
    } else {
        p_list->tail->next = p_new_node;
        p_new_node->prev = p_list->tail;
        p_list->tail = p_new_node;
    }
}

void push_node_after(list_t *p_list, unsigned int prev_node, unsigned char *p_packet, unsigned short packet_size){
    node_t *p_current_node = get_node(p_list,prev_node);
    node_t *p_next_node = p_current_node->next;

    node_t *p_new_node = malloc(sizeof(struct node_t));
    p_new_node->p_packet = malloc(packet_size);
    if (p_packet == NULL){
        printf("List element allocation error\n");
        return;
    }

    memcpy(p_new_node->p_packet, p_packet, packet_size);
    p_new_node->packet_size = packet_size;

    //if this is a new last node
    if (p_next_node == NULL){
        p_list->tail = p_new_node;
        p_new_node->next = NULL;
    } else {
        p_new_node->next = p_next_node;
    }
    p_current_node->next = p_new_node;
    p_new_node->prev = p_current_node;
}


void delete_node(list_t *p_list, unsigned int node){
    node_t *p_current_node = get_node(p_list, node);
    node_t *p_next_node = p_current_node->next;
    node_t *p_prev_node = p_current_node->prev;

    if(p_current_node == NULL){ return; }

    if(p_prev_node == NULL){
        p_list->head = p_next_node;
    }else if(p_next_node == NULL){
        p_list->tail = p_prev_node;
        p_prev_node->next = NULL;
    }else{
        p_prev_node->next = p_next_node;
        p_next_node->prev = p_prev_node;
    }

    if (p_current_node->p_packet != NULL){free(p_current_node->p_packet);}
    free(p_current_node);
}

