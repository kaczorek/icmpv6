//
// Created by kaczor on 27.11.17.
//

#ifndef ICMPV6_LIST_H
#define ICMPV6_LIST_H


typedef struct node_t node_t;
struct node_t{
    node_t *prev;
    unsigned char *p_packet;
    unsigned short packet_size;
    node_t *next;
};

typedef struct{
    node_t *head; // first node
    node_t *tail; // last node
} list_t;

list_t* create_list(void);
void delete_list(list_t *p_list);
node_t *get_node(list_t *p_list, unsigned int n);
unsigned int get_iterator(list_t *p_list);
void push_node(list_t *p_list, unsigned char *p_packet, unsigned short packet_size);
void push_node_after(list_t *p_list, unsigned int prev_node, unsigned char *p_packet, unsigned short packet_size);
int print_list(list_t *list);
void delete_node(list_t *p_list, unsigned int node);


#endif //ICMPV6_LIST_H
