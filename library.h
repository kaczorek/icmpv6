#ifndef ICMPV6_LIB_LIBRARY_H
#define ICMPV6_LIB_LIBRARY_H

#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <netinet/ether.h>
#include <net/if.h>

struct icmp_v6_pseudo_header_t {
    struct in6_addr ip_v6_src;
    struct in6_addr ip_v6_dst;
    unsigned int icmp_v6_len;
    unsigned char zeros[3];
    unsigned char next_header;
};

void find_interface_and_address_by_user_input_interface_name(char *p_interface_name, struct in6_addr *p_ip_v6_address);
void set_ipv6_version_by_user_input(struct ip6_hdr *p_ip_v6_header);
void set_ipv6_payload_length_by_user_input(struct ip6_hdr *p_ip_v6_header);
void set_ipv6_hops_limit_by_user_input(struct ip6_hdr *p_ip_v6_header);
void set_ipv6_src_and_dst_addresses_by_user_input(struct ip6_hdr *p_ip_v6_header, struct in6_addr *p_default_address);

void set_icmp_v6_type_and_code_by_user_input(struct icmp6_hdr *p_icmp_v6_header);
void set_icmp_v6_message_body_by_user_input(struct icmp6_hdr *p_icmp_v6_header);


int create_icmp_v6_data_by_user_input(unsigned char **p_icmp_v6_data, unsigned short *p_icmp_v6_data_size);

int create_icmp_v6_packet(struct ip6_hdr *p_ip_v6_header,
                          struct icmp6_hdr *p_icmp_v6_header,
                          unsigned char *p_icmp_v6_data,
                          unsigned short icmp_v6_data_size,
                          unsigned char **p_out_ip_packet,
                          unsigned short *p_out_ip_packet_size);

int set_icmp_v6_checksum(struct ip6_hdr *p_ip_v6_header, unsigned char *p_full_ip_packet,
                         unsigned short icmp_v6_data_size);

int send_ethernet_packet(char *p_interface_name, unsigned char *p_ip_packet, unsigned int ip_packet_size);

int fill_socket_and_ethernet_header_params(struct sockaddr_ll *p_socket_address,
                                           struct ether_header *p_ethernet_header,
                                           char *p_interface_name,
                                           int socket_id);

unsigned int calculate_sum(unsigned short *p_buffer, unsigned int size);

#endif