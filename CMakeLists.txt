cmake_minimum_required(VERSION 3.8)
project(icmpv6)

set(CMAKE_C_STANDARD 99)
link_libraries(dl)
set(SOURCE_FILES main.c list.c list.h)
add_executable(icmpv6 ${SOURCE_FILES})